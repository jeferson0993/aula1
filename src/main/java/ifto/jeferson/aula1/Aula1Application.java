package ifto.jeferson.aula1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * NOTA: Seu projeto deve responder 
 * “Olá SEU_NOME, bem vindo a disciplina de PWEBII” 
 * para às solicitações enviadas para 
 * http://localhost:8080/aula?aluno=SEU_NOME
 */

@SpringBootApplication
@RestController
public class Aula1Application {

	public static void main(String[] args) {
		SpringApplication.run(Aula1Application.class, args);
	}

	@GetMapping("/aula")
	public String aula(@RequestParam(value = "aluno", defaultValue = "Jeferson") String nome) {
		return String.format("Olá %s, bem vindo a disciplina de PWEBII", nome);
	}

}
